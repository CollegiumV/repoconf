repoconf
========
Configures package repositories

Supported Operating Systems
---------------------------
* CentOS 7
* Void Linux

Requirements
------------
No Requirements

Role Variables
--------------
None

Role Defaults
-------------
```
---
repositories: <hash> repositories for each distribution
  CentOS: <list> repositories for CentOS
    - name: <string> epel
      description: <string> EPEL YUM repo
      baseurl: <string> https://download.fedoraproject.org/pub/epel/$releasever/$basearch/
  void:
    - name: Repository Main
      description: Main internal repository
      baseurl: http://repo.collegiumv.org/current
      cost: 00
    - name: Repository Multilib Nonfree
      description: Multilib nonfree internal repository
      baseurl: http://repo.collegiumv.org/current/multilib/nonfree
      cost: 10
    - name: Repository Multilib
      description: Multilib internal repository
      baseurl: http://repo.collegiumv.org/current/multilib
      cost: 10
    - name: Repository nonfree
      description: Nonfree internal repository
      baseurl: http://repo.collegiumv.org/current/nonfree
      cost: 10
    - name: CollegiumV
      description: Custom builder main repository
      baseurl: http://pkgs.collegiumv.org/
      cost: 20
    - name: CollegiumV nonfree
      description: Custom builder nonfree repository
      baseurl: http://pkgs.collegiumv.org/nonfree
      cost: 20
```

Dependencies
------------
No Dependencies

Testing
-------
Tests can be found in the `tests` directory. Current tests validate the following:

* yamllint
* Syntax check
* Provisioning

### Local Testing
Local testing can be done by running the `docker-compose.yml` file found in the `tests` directory

### CI Testing
CI testing configuration can be found in `.gitlab-ci.yml` in the root directory

License
-------
ISC

Author Information
------------------
CV Admins <cvadmins@utdallas.edu>
