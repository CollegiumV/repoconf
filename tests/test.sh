#!/bin/sh -e
# Lint
echo "Linting role..."
yamllint ../
# Syntax check
echo "Checking role syntax..."
ansible-playbook --syntax-check all.yml
# Ansible Playbook
echo "Running role..."
ansible-playbook all.yml
